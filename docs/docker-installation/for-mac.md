 # For macOS

 ## Step 1: Download Docker Desktop

 In the web browser of your choice, navigate to [docker.com/get-started](https://www.docker.com/get-started/) and click the blue button that says, "Download for Mac." Ensure the correct chip is selected (Intel vs. Apple).

  ![Download Docker for Mac](./img/download-docker-for-mac.png)


## Step 2: Setup

We'll need to make a few tweaks to Docker to optimize our experience. Start by opening Docker Desktop:

![Open Docker for Desktop App](./img//docker-desktop-open.png)

And click the "gear" found in the top right of the window.

:::note

A fresh installation of Docker Desktop won't have anything in the container list. That'll change once we start some up.

:::

### General Preferences

Upon opening the preferences, you'll see the General tab:

![General Preferences for Docker Desktop](./img/docker-desktop-prefs-general.png)

:::danger Important

Please make sure *"Use Docker Compose V2"* is ***selected***

:::

### Experimental Features

Next, click the "Experimental Features" tab in the sidebar on the left.

Both checkboxes should be checked

:::info

If you don't see "Enable VirtioFS Accelerated directory sharing, please update both macOS and Docker for Mac to their latest versions

:::

![Docker for Desktop Experimental Features](./img/docker-desktop-prefs-experimental-features.png)

Be sure to press "Apply &amp; Restart"

You should be good to set up your first project, [NGINX Proxy](/docs/docker-usage/nginx-proxy/)