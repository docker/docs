# Getting Started with NGINX proxy
Here are the basics of Docker networking:

Docker has an internal network that our containers use to communicate with each other. To interact with that network, we need to set up a "reverse proxy," or something that acts as a middleman between your `localhost` network and all of your containers' networks. In a basic docker setup, you can forward the port your docker container is listening on, say port `80`, to port `8080` on your computer's local network. Docker bridges the networks using ports. 

This means that any time you want to run multiple websites from Docker, they each need a unique port, which quickly gets overwhelming. `nginx-proxy` works to remediate this by using a "middleman" container to forward ports 80 and 443, then proxies the requests from those ports to internal containers. This setup is known as "reverse proxying" and is very popular as it enables many production-level deployment methodologies, such as load balancing and vhosts.

In this chapter, we will set up NGINX proxy and its prerequisites.