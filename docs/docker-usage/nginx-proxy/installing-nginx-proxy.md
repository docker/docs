# Installing NGINX Proxy


In a new terminal window, you'll need to clone `nginx-proxy`:

```bash
git clone git@gitlab.msu.edu:docker/nginx-proxy
```

Next, we will want to change our current directory to our newly cloned repo and we'll also pull the latest version of `nginx-proxy`.

```bash
cd nginx-proxy
docker compose pull
```

:::info

Make sure Docker is running!

:::

Finally, start it up! 

```bash
docker compose up -d
```