# mkcert: local SSL made easy

The main reason to use `nginx-proxy` is the fact it configures HTTPS automatically. We have to generate a self-signed cert, put it in a folder, and `nginx-proxy` does the rest.

Of course, generating a self-signed certificate isn't the most straightforward task ever, plus going the manual route, google chrome will give us a nasty insecure error every time we try to go to our site. This nifty tool called [mkcert](https://github.com/FiloSottile/mkcert) makes the process of creating certs so much easier.

## Installing mkcert

For macOS, you'll want to install it using [homebrew](brew.sh). If you don't yet have the package manager, now is a great time to install it! Alternatively, you can build from source or use [MacPorts](https://www.macports.org/)

```bash
brew install mkcert
brew install nss # if you use Firefox
```

For windows, you're either going to want to use [scoop](http://scoop.sh/) or [chocolatey](https://chocolatey.org/)[^1]

```powershell
choco install mkcert
```

OR

```powershell
scoop bucket add extras
scoop install mkcert
```

OR, if you don't want the hassle of long-term convenience, use a [binary](https://github.com/FiloSottile/mkcert/releases)

After installation of mkcert, you can run the command below, which will install a local [Certificate Authority](https://en.wikipedia.org/wiki/Certificate_authority), allowing mkcert and the browsers to authenticate certs against that. 

```bash
mkcert -install
```

## mkcert security considerations
It is worth noting that mkcert is secure. Your local certs will only work on *your* machine, thanks to your personal CA.

## Creating our Certs

**TASK**: You'll want to change the directory to your `nginx-proxy` repo in a terminal window. Then you'll want to move to the `SSL` folder, then run the mkcert command. Here's an example of what it should look like:

```bash
cd ~/Developer/nginx-proxy     # This is the location of the nginx-proxy repo
cd SSL                         # Notice that we are `cd-ing` into the SSL folder before 
                                 # utilizing the mkcert command
mkcert -key-file broad.test.key -cert-file broad.test.crt broad.test "*.broad.test"
```

## Updating the Hosts File

Usually, a domain name server (DNS) translates a domain name to an IP address in a typical internet web application. All our "domain names" we want to serve on our local machine are all going to the same IP `127.0.0.1`. So, we tell the machine where to find the mapping for our domain name. 

### macOS/Linux

In a terminal session:

```bash
echo "127.0.0.1 broad.test" | sudo tee -a /etc/hosts
echo "127.0.0.1 internal.broad.test" | sudo tee -a /etc/hosts
```

### Windows

Open PowerShell in *Administrator mode* and run:

```powershell
Add-Content -Path $env:windir\System32\drivers\etc\hosts -Value "`n127.0.0.1`tbroad.test" -Force
Add-Content -Path $env:windir\System32\drivers\etc\hosts -Value "`n127.0.0.1`tinternal.broad.test" -Force
```


# Footnotes

[^1]: These days, I tend to prefer scoop over chocolatey. Chocolatey makes you run it in admin mode whenever you want to install it, and scoop doesn't do that.

<!-- [^1]: This article has a great write up for why you would want a reverse proxy for docker: http://jasonwilder.com/blog/2014/03/25/automated-nginx-reverse-proxy-for-docker/
[^2]: According to [RFC-6761](https://tools.ietf.org/html/rfc6761), four TLDs (top level domains) are reserved for development: `.example`, `.invalid`, `.localhost`, and `.test`. `.local` really *shouldn't* be used for local development. According to [RFC-6762](https://tools.ietf.org/html/rfc6762), `.local` is used for [Multicast DNS](https://en.wikipedia.org/wiki/Multicast_DNS), allowing computers on your local network to find eachother using `*.local` domains. I suggest using `.test`. Google owns both `.app` and `.dev`, so please do not use those domains either.
[^3]: I recommend [git-tower](git-tower.com) as it's available for both Windows and macOS, and it looks charming on macOS. Other GUIs: https://git-scm.com/downloads/guis
[^4]: This will fill up the more projects you create. By the end of the guide, you'll have two!
[^6]: This folder will be the *cloned* repository, not the parent of that directory, but the repo itself. Another way of looking at it is the folder that contains the `docker-compose.yml` file.
[^7]: Of course, the project path will be different on your machine! -->